﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using DG.Tweening;
using TMPro;

public enum Team
{
    None = 0,
    Black = 1,
    Blue = 2,
}

public enum PlayerState
{
    None = 0,
    Hit = 1,
}

public enum PlayerType
{
    None = 0,
    Quarterback = 1,
    Runningback = 2,

    OffensiveLine = 4,
    WideReciever = 5,

    DefensiveLine = 10,
    Cornerback = 11,
    FreeSafety = 12,
}

public class Player : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public Team team;
    public PlayerType playerType = PlayerType.None;

    private float BASE_FOLLOW_SPEED = 0.06f;
    private float currentFollowSpeed;

    public Vector3 predictedPos = Vector3.zero;
    Cooldown followChangeTimer;

    public Vector3 previousPos;
    public Vector3 calulatedVelocity;

    public int playerNum = 0;

    public RadialProgressBar tackleHp;
    public Rigidbody2D rigidbody;

    public SpriteRenderer targetVisual;
    public TextMeshPro positionText;

    private void Awake()
    {
        Init();
    }

    public void Init()
    {
        tackleHp = Helpers.MakeRadialProgressBar(1f, this.transform, new Vector3(2f, -2f, 0f));
        targetVisual.gameObject.SetActive(false);
    }

    private void Update()
    {
        calulatedVelocity = (transform.position - previousPos) / Time.deltaTime;
        previousPos = transform.position;

        if (Game.Instance.inputState == InputState.Playing)
        {
            switch(playerType)
            {
                case PlayerType.DefensiveLine:
                    if(Vector3.Distance(predictedPos, this.transform.position) < 0.1f)
                        UpdateFollowParameters();

                    this.transform.position = Vector3.MoveTowards(this.transform.position, predictedPos, currentFollowSpeed);

                    break;
            }

            if(IsBallCarrier() && !InBounds())
            {
                Game.Instance.OutOfBounds();
                return;
            }

            if(IsBallCarrier() && transform.position.y >= 100f)
            {
                Game.Instance.TouchDown();
            }
        }

        float newZ = this.transform.localRotation.eulerAngles.z;
        newZ = newZ >= 180f ? newZ + Time.deltaTime * 10f : newZ - Time.deltaTime * 10f;
        this.transform.SetLocalRotationZ(newZ);
    }

    TemporarySpriteRenderer tsr;
    public void UpdateFollowParameters()
    {
        if (tsr != null)
            tsr.gameObject.DestroySelf();

        tsr = null;

        Vector3 ballCarrierPos = InputManager.Instance.controlledPlayer.transform.position;
        currentFollowSpeed = GetRandomFollowSpeed();
        followChangeTimer.StartCooldown(Helpers.RandomFloatFromRangeInclusive(1f, 3f));

        Vector3 ballCarrierVelocity = InputManager.Instance.controlledPlayer.calulatedVelocity;

        predictedPos = GetPredictedPosition(ballCarrierPos, InputManager.Instance.controlledPlayer.calulatedVelocity, followChangeTimer.GetTimespan());
        tsr = Helpers.SpawnTempSprite("cancel", Game.Instance.transform, predictedPos, followChangeTimer.GetTimespan(), 0.25f);
    }

    public Vector3 GetPredictedPosition(Vector3 targetPos, Vector3 targetVelocity, float howLong)
    {
        return targetPos + (targetVelocity * howLong);
    }

    float GetRandomFollowSpeed()
    {
        float currentFollowSpeed = BASE_FOLLOW_SPEED;
        currentFollowSpeed += Helpers.RandomFloatFromRangeInclusive(-BASE_FOLLOW_SPEED * 0.5f, BASE_FOLLOW_SPEED * 0.5f);
        return currentFollowSpeed;
    }


    bool InBounds()
    {
        Rect rect = new Rect(-27f, -20f, 54f, 140f);

        return (rect.Contains(this.transform.position));
    }

    public void SetPlayerType(PlayerType playerType)
    {
        this.playerType = playerType;

        switch(playerType)
        {
            case PlayerType.DefensiveLine:
                followChangeTimer = Helpers.MakeGameObjectWithComponent<Cooldown>(this.transform);
                followChangeTimer.callback = UpdateFollowParameters;
                followChangeTimer.updateCheckFunction = ShouldUpdate;
                followChangeTimer.StartCooldown(Helpers.RandomFloatFromRangeInclusive(0f, 0.5f));
                break;
            case PlayerType.Quarterback:
                tackleHp.Init(0.1f);
                break;
            case PlayerType.Runningback:
                tackleHp.Init(1f);
                break;
        }

        positionText.text = GetPositionAbbreviation(playerType);
    }

    internal void PreSnapPlayerSetup(float playSetupTime)
    {
        transform.DORotate(Vector3.zero, playSetupTime);
        tackleHp.Reset();

        switch (playerType)
        {
            case PlayerType.OffensiveLine:
                tackleHp.Init(Helpers.RandomFloatFromRangeInclusive(1f, 5f));
                rigidbody.mass = Helpers.RandomFloatFromRangeInclusive(1f, 10f);
                break;
        }

        transform.DOMove(Field.Instance.GetPlayerPosition(Game.Instance.GetSnapPosition(), playerType), playSetupTime);
    }

    bool ShouldUpdate()
    {
        return Game.Instance.inputState == InputState.Playing;
    }

    public void SetTeam(Team team)
    {
        this.team = team;
        this.spriteRenderer.color = GetTeamColor(team);

        if (team == Team.Black)
        {
            Game.Instance.blackTeam.Add(this);
            this.playerNum = Game.Instance.blackTeam.Count;
            SetupPlayerAttributes();
        }

        if (team == Team.Blue)
        {
            Game.Instance.blueTeam.Add(this);
            this.playerNum = Game.Instance.blueTeam.Count;
        }

        this.gameObject.name = team.ToString() + " " + playerNum;
    }

    public static Color GetTeamColor(Team team)
    {
        switch(team)
        {
            case Team.Black:
                return Color.black;
            case Team.Blue:
                return Color.blue;
        }

        return Color.cyan;
    }

    internal void SetFollowSpeed(float v)
    {
        currentFollowSpeed = 0f;
    }

    //private void OnTriggerEnter2D(Collider2D collider)
    //{
    //    Debug.Log(collider.name);

    //    Player player = collider.GetComponent<Player>();

    //    HandleCollision(new List<Player>() { this, player });
    //}

    //void OnCollisionEnter2D(Collision2D other)
    //{
    //    Debug.Log(other.collider.name);

    //    Player player = other.collider.GetComponent<Player>();

    //    HandleCollision(new List<Player>() { this, player });
    //}

    //void HandleCollision(List<Player> players)
    //{
    //    switch(Game.Instance.inputState)
    //    {
    //        case InputState.Playing:
    //            if(players.Any(x => x.IsBallCarrier()))
    //            {
    //                Player ballCarrier = players.First(x => x.IsBallCarrier());
    //                players.Remove(ballCarrier);
    //                Game.Instance.BallCarrierCollision(ballCarrier, players[0]);
    //            }
    //            break;
    //    }
    //}

    Player contactablePlayer;
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (Game.Instance.inputState != InputState.Playing)
            return;

        //Debug.Log(collision.otherCollider.name);

        contactablePlayer = collision.collider.GetComponent<Player>();

        if (contactablePlayer == null)
            return;

        if(contactablePlayer.team == this.team)
            return;

        if (this.ContactablePlayer())
        {
            ProcessContact(this);
        }
        else
        {
            if (contactablePlayer.ContactablePlayer())
                ProcessContact(contactablePlayer);
        }
    }

    public bool ContactablePlayer()
    {
        return InputManager.Instance.controlledPlayer == this || this.playerType == PlayerType.OffensiveLine;
    }

    void ProcessContact(Player player)
    {
        if (this.playerType == PlayerType.Quarterback)
        {
            tackleHp.AdjustBarValue(-Time.deltaTime);

            if (tackleHp.IsEmpty())
                Game.Instance.BallCarrierTackled();
        }

        if (this.playerType == PlayerType.OffensiveLine)
        {
            tackleHp.AdjustBarValue(-Time.deltaTime);
            if (tackleHp.IsEmpty())
                rigidbody.mass = 1f;
        }
    }

    public bool IsBallCarrier()
    {
        return InputManager.Instance.controlledPlayer == this;
    }

    void SetupPlayerAttributes()
    {
        switch(playerNum)
        {
            case 4:
            case 5:
                BASE_FOLLOW_SPEED *= 1.3f;
                break;
        }
    }

    public string GetPositionAbbreviation(PlayerType ptype)
    {
        switch(ptype)
        {
            case PlayerType.Quarterback:
                return "QB";
            case PlayerType.Runningback:
                return "RB";
            case PlayerType.OffensiveLine:
                return "OL";
            case PlayerType.DefensiveLine:
                return "DL";
            case PlayerType.Cornerback:
                return "CB";
            case PlayerType.WideReciever:
                return "WR";
            case PlayerType.FreeSafety:
                return "FS";
        }
        return "?";
    }
}
