﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using TMPro;

public class PingPongTextFader : MonoBehaviour {
    public TextMeshPro textMesh;

    public Ease ease = Ease.Linear;

    private Color startColor;
    public float duration = 1f;

    public Color endColor;

    Tweener tween;

    void OnEnable()
    {
		startColor = textMesh.color;
        //endColor = Helpers.AlphaAdjustedColor(startColor, 0f);
		tween = DOTween.To(() => textMesh.color, x => textMesh.color = x, endColor, duration).ChangeStartValue(startColor).SetAutoKill(false).SetLoops(-1, LoopType.Yoyo);
        tween.Play();
    }

    void OnDisable()
    {
        tween.Kill();

		textMesh.color = startColor;
    }
}
