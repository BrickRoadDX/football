﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TemporarySpriteRenderer : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public Temporary temporary;

    public Vector3 velocity = Vector3.zero;

    private void Update()
    {
        if (velocity != Vector3.zero)
            this.transform.position += velocity;
    }
}
