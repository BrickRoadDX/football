﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class VolumeControl : MonoBehaviour
{
    public Slider scrollbar;

    bool initialized = false;
    void Start()
    {
        AudioListener.volume = 1f;

        float soundVolume = (float)SaveManager.GetInt("soundVolume", 25) / 100f;
        scrollbar.value = soundVolume;
        Helpers.LoadSoundValues();
        initialized = true;
    }

    void OnEnable()
    {
        scrollbar.onValueChanged.AddListener(delegate { HandleScrollbarScrolled(); });
    }

    bool scrolledSound = false;
    void HandleScrollbarScrolled()
    {
        if (!initialized)
            return;

        UpdateSoundVolumes();

        scrolledSound = true;
    }

    void Update()
    {
        if (Input.GetMouseButtonUp(0) && scrolledSound)
        {
            scrolledSound = false;
            SoundManager.Instance.PlaySound("gainVP");
        }
    }

    void UpdateSoundVolumes()
    {
        SaveManager.SetInt("soundVolume", (int)(scrollbar.value * 100f));
        SaveManager.SetInt("musicVolume", (int)(scrollbar.value * 100f));

        SoundManager.SetSoundVolume(scrollbar.value);
        MusicManager.SetMusicVolume(scrollbar.value);
    }
}
