﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class PingPongMover : MonoBehaviour {
	public Ease ease = Ease.Linear;

	public Vector3 offset = Vector3.zero;
	private Vector3 startLocation;
	public float duration = 1f;

	Tweener tween;

    public bool once = false;

	void OnEnable()
	{
		startLocation = transform.localPosition;
		tween = transform.DOLocalMove(startLocation + offset, duration).SetUpdate(false);
		tween.SetEase(ease).SetLoops(-1, LoopType.Yoyo).ChangeStartValue(startLocation);

        if (once)
            tween.SetLoops(1);

		tween.Play();
	}
	
	void OnDisable()
	{
		tween.Kill();

		transform.localPosition = startLocation;
	}

	void Update()
	{
		if(!tween.IsPlaying() && Time.timeScale > 0f)
		{
			tween.Play();
		}

		if(tween.IsPlaying() && Time.timeScale <= 0f)
		{
			tween.Pause();
		}

	}
}
