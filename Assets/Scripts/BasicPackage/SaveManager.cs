﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;

public static class SaveManager
{
    public static float saveDelay = 0f;

	private static Dictionary<string, int> intValues = new Dictionary<string, int>();
	private static Dictionary<string, string> stringValues = new Dictionary<string, string>();

    public static PersistentGameController gameController;

    static void TryInit()
    {
        if(gameController == null)
        {
            gameController = PersistentGameController.Instance;
        }
    }

	public static string GetSavesPath()
	{
		string path = Application.dataPath + "/SavesDir";
		return path;
	}

    public static string GetIncrementalBackupSavesPath()
    {
        string path = Application.dataPath + "/SavesDir/daily";
        return path;
    }

    public static string GetIntsPath(int fileNum)
    {
        string preFix = "";

        if (fileNum > 0)
            preFix = "backup_";

        if(fileNum == -1) //dailybackup
            return GetIncrementalBackupSavesPath() + string.Format("/{0}_axesInts.sav", DateTime.Now.DayOfWeek.ToString());

        return GetSavesPath() + string.Format("/{0}axesInts.sav", preFix);
    }

    public static string GetStringsPath(int fileNum)
    {
        string postFix = "";

        if (fileNum > 0)
            postFix = "backup_";

        if (fileNum == -1) //dailybackup
            return GetIncrementalBackupSavesPath() + string.Format("/{0}_axesStrings.sav", DateTime.Now.DayOfWeek.ToString());

        return GetSavesPath() + string.Format("/{0}axesStrings.sav", postFix);
    }

    public static void SaveGame(bool overrideSaveDelay = false)
    {
        TryInit();

        #if !UNITY_EDITOR && !UNITY_STANDALONE
	        return;
        #endif

        Directory.CreateDirectory(GetSavesPath());
        Directory.CreateDirectory(GetIncrementalBackupSavesPath());

        WriteFile(0);
        WriteFile(1);
        WriteFile(-1);

        PersistentGameController.saveWaiting = false;
        saveDelay = 1f;
    }

    static bool WriteFile(int fileNum)
    {
        return WriteFile(GetIntsPath(fileNum), GetStringsPath(fileNum));
    }

    static bool WriteFile(string intsPath, string stringsPath)
    {
        FileStream fs = null;
        BinaryWriter bw = null;

        FileStream fs2 = null;
        BinaryWriter bw2 = null;
        bool success = true;

        try
        {
            //ints
            File.Delete(intsPath);

            fs = File.OpenWrite(intsPath);
            bw = new BinaryWriter(fs);
            foreach (KeyValuePair<string, int> kvp in intValues)
            {
                bw.Write(kvp.Key);
                bw.Write(kvp.Value);
            }

            bw.Close();
            fs.Close();

            //strings
            File.Delete(stringsPath);

            fs2 = File.OpenWrite(stringsPath);
            bw2 = new BinaryWriter(fs2);
            foreach (KeyValuePair<string, string> kvp in stringValues)
            {
                bw2.Write(kvp.Key);
                bw2.Write(kvp.Value);
            }

            bw2.Close();
            fs2.Close();
        }
        catch (Exception e)
        {
            Debug.Log(e);
            success = false;
        }
        finally
        {
            if (fs != null)
                fs.Close();

            if (bw != null)
                bw.Close();

            if (fs2 != null)
                fs2.Close();

            if (bw2 != null)
                bw2.Close();
        }

        return success;
    }

    public static bool triedRestoringFromFileTwo = false;
    public static bool fileTwoCorrupted = false;
    public static bool directoryExistsBeforeLoad = false;
    public static void LoadGame()
    {
        TryInit();

        bool directoryExistsBeforeLoad = Directory.Exists(GetSavesPath());
        Directory.CreateDirectory(GetSavesPath());

        if (File.Exists(GetIntsPath(0)) && File.Exists(GetStringsPath(0)) && ReadFile(0)) //try read file if succeed, save backup
        {
            WriteFile(1); //save backup
        }
        else
        {
            if (ReadFile(1))
            {
                if (!triedRestoringFromFileTwo)
                {
                    triedRestoringFromFileTwo = true;
                    //if we read the backup, great, try to save it to the normal place
                    if (WriteFile(0))
                    {
                        if (directoryExistsBeforeLoad)
                            Debug.LogError("Normal file could not be read, restored from backup.");
                            //PopupManager.Instance.ShowPopupOneButton("Normal file could not be read, restored from backup.");
                    }
                    else
                    {
                        if(directoryExistsBeforeLoad)
                            //PopupManager.Instance.ShowPopupOneButton("Normal save file could not be read, backup loaded but could not be retored. Please make sure your save files are not open.\n\nIf you have done this, restart your system and try again, or contact support at braingoodgames@gmail.com.", Application.Quit);
                            Debug.LogError("Normal save file could not be read, backup loaded but could not be retored. Please make sure your save files are not open.\n\nIf you have done this, restart your system and try again, or contact support at braingoodgames@gmail.com.");
                    }
                }
            }
            else
            {
                Debug.LogError("Normal file and backup save files could not be read, please make sure your save files are not open.\n\nIf you have done this, restart your system and try again, or contact support at braingoodgames@gmail.com.");
                //PopupManager.Instance.ShowPopupOneButton("Normal file and backup save files could not be read, please make sure your save files are not open.\n\nIf you have done this, restart your system and try again, or contact support at braingoodgames@gmail.com.", Application.Quit);
                //both files are corrupt
            }
        }
    }

    static bool ReadFile(int fileNum)
    {
        return ReadFile(GetIntsPath(fileNum), GetStringsPath(fileNum));
    }

    private static bool ReadFile(string intsPath, string stringsPath)
    {
        FileStream fs = null;
        BinaryReader br = null;
        bool success = true;

        try
        {
            Directory.CreateDirectory(GetSavesPath());
            intValues.Clear();
            stringValues.Clear();

            //ints
            if (File.Exists(intsPath))
            {
                fs = File.OpenRead(intsPath);
                br = new BinaryReader(fs);

                bool done = false;

                while (!done)
                {
                    if (br.PeekChar() == -1)
                    {
                        done = true;
                    }
                    else
                    {
                        string key = br.ReadString();
                        int value = br.ReadInt32();
                        if (!intValues.ContainsKey(key))
                        {
                            intValues.Add(key, value);
                        }
                    }
                }

                fs.Close();
                br.Close();
            }

            //strings
            if (File.Exists(stringsPath))
            {
                fs = File.OpenRead(stringsPath);
                br = new BinaryReader(fs);

                bool done = false;

                while (!done)
                {
                    if (br.PeekChar() == -1)
                    {
                        done = true;
                    }
                    else
                    {
                        string key = br.ReadString();
                        string value = br.ReadString();
                        if (!stringValues.ContainsKey(key))
                        {
                            stringValues.Add(key, value);
                        }
                    }
                }
                fs.Close();
                br.Close();
            }

        }
        catch (Exception e)
        {
            Debug.LogWarning(e);
            success = false;
        }
        finally
        {
            if (fs != null)
                fs.Close();

            if (br != null)
                br.Close();
        }

        return success;
    }

    public static void SetInt(string key, int value)
	{
		LoadGame();
		if (intValues.ContainsKey(key))
		{
			intValues[key] = value;
		}
		else
		{
			intValues.Add(key, value);
		}
		SaveGame();
	}


	public static int GetInt(string key, int defaultValue = -1)
	{
		LoadGame();
		if (HasKey(key))
			return intValues[key];

		return defaultValue;
	}

	public static void SetBool(string key, bool value)
	{
		LoadGame();
		if (intValues.ContainsKey(key))
		{
			intValues[key] = value ? 1 : 0;
		}
		else
		{
			intValues.Add(key, value ? 1 : 0);
		}
		SaveGame();
	}


	public static bool GetBool(string key, bool defaultValue = false)
	{
		LoadGame();
		if (HasKey(key))
			return intValues[key] == 1;

		return defaultValue;
	}

	public static void SetString(string key, string value)
	{
		LoadGame();
		if (stringValues.ContainsKey(key))
		{
			stringValues[key] = value;
		}
		else
		{
			stringValues.Add(key, value);
		}
		SaveGame();
	}


	public static string GetString(string key, string defaultValue = "")
	{
		LoadGame();
		if (HasKey(key))
			return stringValues[key];

        return defaultValue;
    }

	public static bool HasKey(string key)
	{
		LoadGame();
		return intValues.ContainsKey(key) || stringValues.ContainsKey(key);
	}

	public static void DeleteAll()
	{
		intValues.Clear();
		stringValues.Clear();
		SaveGame();
	}
}