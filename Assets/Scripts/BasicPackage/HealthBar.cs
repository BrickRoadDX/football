﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour {

    private int maxHp;
    private int currentHp;

    private List<HealthBarPip> healthBarPips = new List<HealthBarPip>();

    public void Init(int hp, string prefabName, Color color = default(Color))
    {
        maxHp = hp;
        currentHp = hp;

        for(int i = 0; i< hp; i++)
        {
            HealthBarPip hbp = Helpers.CreateInstance<HealthBarPip>(prefabName, this.transform, true);
            healthBarPips.Add(hbp);

            if(color != default(Color))
                hbp.healthBarFill.color = color;
        }

        UpdatePositions();
        UpdateHealth(maxHp);
    }

    void UpdatePositions()
    {
        float xOffset = 0f;
        if (maxHp == 2)
            xOffset = 4f;

        float yOffset = 0f;
        foreach(HealthBarPip pip in healthBarPips)
        {
            pip.transform.SetLocalPositionX(xOffset);
            pip.transform.SetLocalPositionY(yOffset);
            xOffset += pip.X_OFFSET;

            //if(xOffset > 31f)
            //{
            //    xOffset = 0f;
            //    yOffset += pip.X_OFFSET;
            //}
        }
    }

    public void UpdateHealth(int currentHp)
    {
        this.currentHp = currentHp;
        for(int i = 0; i < maxHp; i++)
        {
            healthBarPips[i].healthBarFill.gameObject.SetActive(currentHp > i);
        }

        this.gameObject.SetActive(this.currentHp > 0);

    }

    internal void AdjustHp(int amount)
    {
        SetHp(currentHp + amount);
    }

    internal void SetHp(int newHp)
    {
        currentHp = Mathf.Min(newHp, maxHp);

        UpdateHealth(currentHp);
    }

    public bool IsDead()
    {
        return currentHp < 1;
    }

    internal bool IsMax()
    {
        return currentHp == maxHp;
    }

    public int CurrentHp()
    {
        return currentHp;
    }
}
