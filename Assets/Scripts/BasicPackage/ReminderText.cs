﻿using UnityEngine;
using System.Collections;
using TMPro;

public class ReminderText : MonoBehaviour {
	public TextMeshPro text;
	public PingPongTextFader fader;
	public PingPongMover mover;
}
