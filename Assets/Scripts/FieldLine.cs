﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FieldLine : MonoBehaviour
{
    public List<TextMeshPro> lineNumbers;
    public SpriteRenderer spriteRenderer;

    public void SetFieldLineNumber(int number)
    {
        foreach (TextMeshPro tmp in lineNumbers)
            tmp.text = number.ToString();

        if(number == 0)
        {
            foreach (TextMeshPro tmp in lineNumbers)
                tmp.text = "";

            spriteRenderer.transform.SetLocalScaleY(0.2f);
        }
    }
}
