﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Field : Singleton<Field>
{
    public List<FieldLine> downDistanceFieldLines = new List<FieldLine>();

    // Start is called before the first frame update
    void Start()
    {
        FieldLine fl;
        int fieldPos = -20;

        fl = Helpers.CreateInstance<FieldLine>("FieldLine", this.transform);
        fl.SetFieldLineNumber(0);
        fl.transform.SetLocalPositionX(0f);
        fl.transform.SetLocalPositionY(-21.16f);
        fieldPos += 20;

        for (int i = 0; i <= 5; i++)
        {
            fl = Helpers.CreateInstance<FieldLine>("FieldLine", this.transform);
            fl.SetFieldLineNumber(i * 10);
            fl.transform.SetLocalPositionX(0f);
            fl.transform.SetLocalPositionY(fieldPos);
            fieldPos += 10;
        }

        for (int i = 4; i >= 0; i--)
        {
            fl = Helpers.CreateInstance<FieldLine>("FieldLine", this.transform);
            fl.SetFieldLineNumber(i * 10);
            fl.transform.SetLocalPositionX(0f);
            fl.transform.SetLocalPositionY(fieldPos);
            fieldPos += 10;
        }

        fieldPos += 20;

        fl = Helpers.CreateInstance<FieldLine>("FieldLine", this.transform);
        fl.SetFieldLineNumber(0);
        fl.transform.SetLocalPositionX(0f);
        fl.transform.SetLocalPositionY(121.16f);
    }

    FieldLine CreateFieldLine(float yardLine, Color lineColor)
    {
        FieldLine fl = Helpers.CreateInstance<FieldLine>("FieldLine", this.transform);
        fl.SetFieldLineNumber(0);
        fl.transform.SetLocalPositionX(0f);
        fl.transform.SetLocalPositionY(yardLine);
        fl.spriteRenderer.color = lineColor;
        return fl;
    }

    public void UpdateFieldLines()
    {
        foreach(FieldLine line in downDistanceFieldLines)
        {
            line.gameObject.DestroySelf();
        }

        downDistanceFieldLines.Clear();

        downDistanceFieldLines.Add(CreateFieldLine(Game.Instance.firstDownLine, Color.yellow * 0.5f));
        downDistanceFieldLines.Add(CreateFieldLine(Game.Instance.yardLine, Color.blue * 0.5f));
    }

    public Dictionary<PlayerType, int> playerCounts = new Dictionary<PlayerType, int>();
    public void ResetPlayerPositions()
    {
        playerCounts.Clear();
    }

    public Vector3 GetPlayerPosition(Vector3 snapPosition, PlayerType pType)
    {
        Vector3 position = Game.Instance.GetSnapPosition();

        int playerIndex = 0;
        if(playerCounts.ContainsKey(pType))
        {
            playerCounts[pType] = playerCounts[pType] + 1;
            playerIndex = playerCounts[pType];
        }
        else
        {
            playerCounts.Add(pType, 0);
        }

        switch (pType)
        {
            case PlayerType.Quarterback:
            case PlayerType.Runningback:
                return position + Vector3.down * 5f; //Quaterback
            case PlayerType.OffensiveLine:
                List<Vector3> oLinePositions = new List<Vector3>()
                {
                    position + Vector3.left * 3f,
                    position + Vector3.left * 1f,
                    position + Vector3.right * 1f,
                    position + Vector3.right * 3f
                };

                position = oLinePositions[playerIndex];
                break;
            case PlayerType.DefensiveLine:
                List<Vector3> dLinePositions = new List<Vector3>()
                {
                    position + Vector3.left * 3f,
                    position + Vector3.left * 1f,
                    position + Vector3.right * 1f,
                    position + Vector3.right * 3f
                };
                position = dLinePositions[playerIndex] + Vector3.up * 1.5f;
                break;
            case PlayerType.Cornerback:
                List<Vector3> cbPositions = new List<Vector3>()
                {
                    position + Vector3.right * 12f + Vector3.up * 7f,
                    position + Vector3.left * 12f + Vector3.up * 7f
                };
                position = cbPositions[playerIndex];
                break;
            case PlayerType.FreeSafety:
                return position + Vector3.up * 10f;
            case PlayerType.WideReciever:
                List<Vector3> wrPositions = new List<Vector3>()
                {
                    position + Vector3.right * 12f,
                    position + Vector3.left * 12f
                };
                position = wrPositions[playerIndex];
                break;
        }

        return position;
    }


}
