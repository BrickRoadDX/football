﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : Singleton<InputManager>
{
    public Player controlledPlayer;

    private PlayerState playerState = PlayerState.None;

    const float ACCELERATION = 0.005f;
    const float FRICTION = 0.95f;
    const float HURT_FRICTION = 0.95f;
    const float BLOOP_BOOST = 0.1f;
    const float TOP_SPEED = 0.05f;

    Vector3 velocity = Vector3.zero;
    Vector3 direction = Vector3.zero;

    List<KeyCode> arrowKeys = new List<KeyCode>() { KeyCode.UpArrow, KeyCode.DownArrow, KeyCode.RightArrow, KeyCode.LeftArrow };
    List<KeyCode> arrowKeysPressed = new List<KeyCode>();

    public Cooldown dashCooldown;

    public Player currentTarget = null;

    public void Init()
    {
        dashCooldown = Helpers.MakeGameObjectWithComponent<Cooldown>(this.transform);
        dashCooldown.StartCooldown(1f, true);

        controlledPlayer = Helpers.CreateInstance<Player>("Player", this.transform);
        controlledPlayer.SetTeam(Team.Blue);

        controlledPlayer.SetPlayerType(PlayerType.Quarterback);
        controlledPlayer.transform.position = Game.Instance.GetSnapPosition();

        CameraFollow.Instance.target = controlledPlayer.transform;
    }

    bool AButtonPressed = false;
    void Update()
    {
#if UNITY_EDITOR
        DebugInput();
#endif
        switch (Game.Instance.inputState)
        {
            case InputState.PreSnap:
                if(Input.GetButtonUp("Fire1"))
                {
                    Game.Instance.GoToState(InputState.Playing);
                }
                return;
            case InputState.Playing:
                AButtonPressed = false;
                UpdateArrowKeysPressed();

                switch (playerState)
                {
                    case PlayerState.None:

                        direction = Vector3.zero;
                        foreach (KeyCode k in arrowKeysPressed)
                        {
                            if (direction == Vector3.zero)
                            {
                                direction = Helpers.GetVectorDirFromDirection(Helpers.GetDirectionFromKeycode(k));
                            }
                            else
                            {
                                direction = Vector3.Lerp(direction, Helpers.GetVectorDirFromDirection(Helpers.GetDirectionFromKeycode(k)), 0.5f);
                            }
                        }

                        if(dashCooldown.IsReady())
                            AButtonPressed = Input.GetButtonDown("Fire1");
                        break;
                }

                if (velocity.magnitude < TOP_SPEED && playerState == PlayerState.None)
                    velocity += direction * ACCELERATION;

                if (AButtonPressed)
                {
                    if(controlledPlayer.playerType == PlayerType.Runningback)
                        Boost();

                    if (controlledPlayer.playerType == PlayerType.Quarterback)
                        ChangeTarget();
                }

                //Debug.Log(velocity.magnitude);

                velocity = velocity * (playerState == PlayerState.Hit ? HURT_FRICTION : FRICTION);

                this.transform.position += velocity;
                return;
        }
    }


    void DebugInput()
    {
        if(Input.GetButtonUp("Submit"))
        {
            Game.Instance.NewGame();
        }
    }

    void Boost()
    {
        //velocity = Vector3.zero;
        dashCooldown.Reset();

        SoundManager.Instance.PlaySound("bloop");

        if (direction != Vector3.zero)
            velocity += direction.normalized * BLOOP_BOOST;
        else
            velocity += velocity.normalized * BLOOP_BOOST;
    }

    public void ChangeTarget()
    {
        SetTarget(GetNextReciever(Team.Blue));
    }

    void SetTarget(Player target)
    {
        if (currentTarget != null)
            currentTarget.targetVisual.gameObject.SetActive(false);

        currentTarget = target;

        if (target == null)
            return;

        currentTarget.targetVisual.gameObject.SetActive(true);
    }

    public List<Player> GetTeamList(Team team)
    {
        List<Player> teamRef = team == Team.Blue ? Game.Instance.blueTeam : Game.Instance.blackTeam;

        return new List<Player>(teamRef);
    }

    Player GetNextReciever(Team team)
    {
        List<Player> teamList = GetTeamList(team);

        if(teamList.Contains(controlledPlayer))
            teamList.Remove(controlledPlayer);

        teamList.RemoveAll(x => x.playerType != PlayerType.WideReciever);

        return teamList.GetNextElement(currentTarget);
    }

    void UpdateArrowKeysPressed()
    {
        arrowKeysPressed.Clear();

        if (Input.GetAxis("Horizontal") > 0f)
            arrowKeysPressed.Add(KeyCode.RightArrow);

        if (Input.GetAxis("Horizontal") < 0f)
            arrowKeysPressed.Add(KeyCode.LeftArrow);

        if (Input.GetAxis("Vertical") > 0f)
            arrowKeysPressed.Add(KeyCode.UpArrow);

        if (Input.GetAxis("Vertical") < 0f)
            arrowKeysPressed.Add(KeyCode.DownArrow);
        //foreach (KeyCode k in arrowKeys)
        //{
        //    if (Input.GetKey(k))
        //        arrowKeysPressed.Add(k);
        //}
    }

    internal void StartPlay()
    {
        velocity = Vector3.zero;
        if (controlledPlayer.playerType == PlayerType.Quarterback)
            ChangeTarget();
    }

    internal void EndPlay()
    {
        SetTarget(null);
    }

}
