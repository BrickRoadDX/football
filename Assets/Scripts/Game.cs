﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum InputState
{
    None = 0,
    PreSnap = 1,
    Playing = 2,
    SettingUpPlay = 3,
}

public enum PlayType
{
    None = 0,
    Run = 1,
    Pass = 2,
}

public class Game : Singleton<Game> {

    public float initialFirstDownLine = 20f;
    public float yardLine = 20f;

    public int down = 1;
    public float firstDownLine = 30f;

    List<FieldLine> fieldLines = new List<FieldLine>();

    public InputState inputState = InputState.None;

    public List<Player> blackTeam = new List<Player>();
    public List<Player> blueTeam = new List<Player>();

    public PlayType currentPlayType = PlayType.None;

    private void Start()
    {
        InputManager.Instance.Init();

        for(int i = 0; i < 4; i++)
            SpawnPlayer(Team.Black, PlayerType.DefensiveLine);

        for (int i = 0; i < 2; i++)
            SpawnPlayer(Team.Black, PlayerType.Cornerback);

        for (int i = 0; i < 1; i++)
            SpawnPlayer(Team.Black, PlayerType.FreeSafety);

        for (int i = 0; i < 4; i++)
            SpawnPlayer(Team.Blue, PlayerType.OffensiveLine);

        for (int i = 0; i < 2; i++)
            SpawnPlayer(Team.Blue, PlayerType.WideReciever);

        NewGame();
    }

    void ShowDownDistance()
    {
        string downText = "";

        switch(down)
        {
            case 1:
                downText = "First";
                break;
            case 2:
                downText = "Second";
                break;
            case 3:
                downText = "Third";
                break;
            case 4:
                downText = "Fourth";
                break;
        }

        downText += " and ";

        int toGo = Mathf.CeilToInt(firstDownLine - yardLine);

        downText += " " + toGo.ToString();

        Helpers.ShowReminderText(downText);
    }

    void FirstDown()
    {
        down = 1;
        initialFirstDownLine = yardLine;
        firstDownLine = yardLine + 10f;
        firstDownLine = Mathf.Min(100f, firstDownLine);

        GoToState(InputState.SettingUpPlay);
    }

    public void TouchDown()
    {
        yardLine = BallPosition().y;
        Helpers.ShowReminderText("TOUCHDOWN!");
    }

    public void OutOfBounds()
    {
        EndPlay();
    }

    public void EndPlay()
    {
        InputManager.Instance.EndPlay();
        yardLine = BallPosition().y;

        if(yardLine >= firstDownLine)
        {
            FirstDown();
        }
        else
        {
            down++;

            if(down > 4)
            {
                Helpers.ShowReminderText("Turnover on downs!");
                GoToState(InputState.None);
                return;
            }
            else
            {
                GoToState(InputState.SettingUpPlay);
            }
        }
        SetupPlay();    
    }

    void SetupPlay(float playSetupTime = 1f)
    {
        Field.Instance.ResetPlayerPositions();

        InputManager.Instance.controlledPlayer.tackleHp.Reset();
        InputManager.Instance.controlledPlayer.transform.DOMove(GetSnapPosition(), playSetupTime);

        PlayerType snapPlayerType = currentPlayType == PlayType.Pass ? PlayerType.Quarterback : PlayerType.Runningback;
        InputManager.Instance.controlledPlayer.SetPlayerType(snapPlayerType);

        foreach (Player a in blackTeam)
        {
            a.PreSnapPlayerSetup(playSetupTime);
        }

        foreach (Player a in blueTeam)
        {
            a.PreSnapPlayerSetup(playSetupTime);
        }

        this.CallActionDelayed(NextPlay, playSetupTime);
        Field.Instance.UpdateFieldLines();
    }

    internal void NewGame()
    {
        currentPlayType = PlayType.Pass;
        yardLine = 20f;
        FirstDown();
        SetupPlay(0f);
    }

    void NextPlay()
    {
        GoToState(InputState.PreSnap);
    }

    Vector3 BallPosition()
    {
        return InputManager.Instance.controlledPlayer.transform.position;
    }

    public void GoToState(InputState state)
    {
        inputState = state;

        switch(inputState)
        {
            case InputState.Playing:
                Helpers.ShowReminderText("GO!");
                foreach(Player player in blackTeam)
                {
                    player.SetFollowSpeed(0f);
                }
                InputManager.Instance.StartPlay();
                break;
            case InputState.PreSnap:
                ShowDownDistance();
                break;
        }
    }

    internal void BallCarrierTackled()
    {
        Helpers.ShowReminderText(GetGainLossText());
        EndPlay();
    }

    public string GetGainLossText()
    {
        int diff = Mathf.FloorToInt(BallPosition().y - yardLine);

        if(diff > 0)
        {
            return string.Format("Gain of {0}", diff);
        }

        return string.Format("Loss of {0}", -diff);
    }

    public Vector3 GetSnapPosition(Vector3 offset = default(Vector3))
    {
        return new Vector3(0f, yardLine, 0f) + offset;
    }

    void SpawnPlayer(Team team, PlayerType controlType)
    {
        Player player = Helpers.CreateInstance<Player>("Player", Field.Instance.transform);
        player.SetTeam(team);
        player.SetPlayerType(controlType);
        player.Init();
    }

}
